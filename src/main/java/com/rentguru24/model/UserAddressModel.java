package com.rentguru24.model;

import com.rentguru24.model.entity.app.UserAddress;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by omar on 10/24/16.
 */
@Repository
public class UserAddressModel extends BaseModel {
    public void insert(UserAddress userAddress){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(userAddress);
        session.getTransaction().commit();
        session.close();
    }
}
