package com.rentguru24.model.entity.app.product.rentable.iface;

import com.rentguru24.model.entity.app.RentType;
import com.rentguru24.model.entity.app.product.iface.Product;

import java.sql.Timestamp;

/**
 * Created by MI on 14-Aug-16.
 */
//

public interface RentalProduct extends Product{



    RentType getRentType();
    double getCurrentValue();
    Timestamp getAvailableFrom();
    Timestamp getAvailableTill();
    boolean isCurrentlyAvailable();
    boolean isReviewStatus();


    void setCurrentValue(double currentValue);
    void setRentFee(double rentFee);
    void setRentType(RentType rentType);
    void setCurrentlyAvailable(boolean currentlyAvailable);
    void setAvailableFrom(Timestamp availableFrom);
    void setAvailableTill(Timestamp availableTill);
}
