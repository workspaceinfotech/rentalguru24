package com.rentguru24.model.entity.app.product.view;

/**
 * Created by Tomal on 8/24/2016.
 */

public class ProductView {

    public static class RentalProductView { }
    public static class MyRentalProductView { }
    public static class MyRentedProductView { }
}
