package com.rentguru24.model;

import com.rentguru24.model.entity.app.product.ProductLiked;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by omar on 8/22/16.
 */
@Repository
public class ProductLikedModel extends BaseModel {
    public ProductLiked insert(ProductLiked productLiked){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(productLiked);
        session.getTransaction().commit();
        session.close();
        return productLiked;
    }

    public boolean isLiked(int productId, int appCridentialId){
        ProductLiked productLiked = this.getAlreadyLiked(productId, appCridentialId);
        return (productLiked!=null)?true:false;
    }

    public void delete(int prductId, int appCridentialId){

        ProductLiked productLiked = this.getAlreadyLiked(prductId, appCridentialId);
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.delete(productLiked);
        session.getTransaction().commit();
        session.close();
    }

    public ProductLiked getAlreadyLiked(int prductId, int appCridentialId){
        Session session = this.sessionFactory.openSession();
        try{
            ProductLiked productLiked = (ProductLiked)
                                        session.createQuery(" FROM ProductLiked productLiked " +
                                                            " WHERE productLiked.rentalProduct.id =:prductId " +
                                                            " AND productLiked.appCredential.id =:appCridentialId")
                                                            .setParameter("prductId", prductId)
                                                            .setParameter("appCridentialId", appCridentialId)
                                                            .setMaxResults(1)
                                                            .uniqueResult();
            return productLiked;
        }finally {
            session.close();
        }
    }
}
