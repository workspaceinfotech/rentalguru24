package com.rentguru24.model;

import com.rentguru24.model.entity.app.product.rentable.ProductLocation;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by mi on 8/29/16.
 */
@Repository
public class ProductLocationModel extends BaseModel {
    public void insert(ProductLocation productLocation) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(productLocation);
        session.getTransaction().commit();
        session.close();
    }
}
