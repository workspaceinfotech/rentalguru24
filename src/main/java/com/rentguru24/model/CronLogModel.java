package com.rentguru24.model;

import com.rentguru24.model.entity.developer.cron.CronLog;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by mi on 10/13/16.
 */
@Repository
public class CronLogModel extends BaseModel {
    public CronLog insert(CronLog cronLog){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(cronLog);
        session.getTransaction().commit();
        session.close();
        return cronLog;
    }
}
