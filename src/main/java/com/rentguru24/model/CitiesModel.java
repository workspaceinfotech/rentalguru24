package com.rentguru24.model;

import com.rentguru24.model.entity.Cities;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by mi on 11/29/16.
 */
@Repository
public class CitiesModel extends BaseModel {



    public Cities getById(int id){
        Session session = this.sessionFactory.openSession();
        try {
            return (Cities)session.createQuery("FROM Cities city WHERE city.id = :id")
                    .setParameter("id", id)
                    .uniqueResult();
        }finally {
            session.close();
        }
    }


}
