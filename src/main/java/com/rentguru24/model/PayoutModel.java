package com.rentguru24.model;

import com.rentguru24.model.entity.app.payments.Payout;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by omar on 8/1/16.
 */
@Repository
public class PayoutModel extends BaseModel {
    public void insert(Payout payout){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(payout);
        session.getTransaction().commit();
        session.close();
    }

}
