package com.rentguru24.model;

import com.rentguru24.model.entity.app.IdentityType;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by omar on 8/2/16.
 */
@Repository
public class IdentityTypeModel extends BaseModel {
    public void insert(IdentityType identityType){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(identityType);
        session.getTransaction().commit();
        session.close();
    }

    public IdentityType getById(int id){
        Session session = this.sessionFactory.openSession();
        return session.get(IdentityType.class, id);
    }
    public boolean isExist(int id){
        Session session = this.sessionFactory.openSession();
        if(session.get(IdentityType.class, id)!=null)
            return true;
        return false;
    }
    public IdentityType getFirst(){
        Session session = this.sessionFactory.openSession();
        String hql = "FROM IdentityType";
        Query query = session.createQuery(hql);
        return (IdentityType)query.setMaxResults(1).uniqueResult();
//        List result =query.list();
//        for (Iterator<IdentityType> iter = result.iterator(); iter.hasNext(); ) {
//            IdentityType element = iter.next();
//            System.out.println(element.getId());
//            System.out.println(element.getName());
//            System.out.println(element.getCreatedDate());
//        }
//        return result;
    }
    public List<IdentityType> getAll(){
        Session session = this.sessionFactory.openSession();
        String hql = "FROM IdentityType";
        Query query = session.createQuery(hql);
        return (List<IdentityType>)query.list();
//        List result =query.list();
//        for (Iterator<IdentityType> iter = result.iterator(); iter.hasNext(); ) {
//            IdentityType element = iter.next();
//            System.out.println(element.getId());
//            System.out.println(element.getName());
//            System.out.println(element.getCreatedDate());
//        }
//        return result;
    }
}
