package com.rentguru24.model;

import com.rentguru24.model.entity.app.TempFile;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by omar on 8/2/16.
 */
@Repository
public class TempFileModel extends BaseModel {

    public void insert(TempFile tempFile){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(tempFile);
        session.getTransaction().commit();
        session.close();
    }

    public com.rentguru24.model.entity.app.TempFile getByToken(long token){
        Session session = this.sessionFactory.openSession();

        try {
            String hql = "FROM TempFile ID WHERE ID.token="+token;
            Query query = session.createQuery(hql);
            return (com.rentguru24.model.entity.app.TempFile)query.uniqueResult();
        }finally {
            session.close();
        }
    }
    public void delete(TempFile tempFile){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.delete(tempFile);
        session.getTransaction().commit();
        session.close();
    }
}
