package com.rentguru24.model;

import com.rentguru24.model.entity.app.product.rentable.RentalProductReturnedHistory;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by mi on 8/27/16.
 */
@Repository
public class RentalProductReturnedHistoryModel  extends BaseModel {

    public void insert(RentalProductReturnedHistory rentalProductReturnedHistory){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(rentalProductReturnedHistory);
        session.getTransaction().commit();
        session.close();

    }
}
