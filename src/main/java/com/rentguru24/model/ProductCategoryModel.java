package com.rentguru24.model;

import com.rentguru24.model.entity.app.product.ProductCategory;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by mi on 11/11/16.
 */
@Repository
public class ProductCategoryModel extends BaseModel {
    public void insert(ProductCategory productCategory){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(productCategory);
        session.getTransaction().commit();
        session.close();
    }
    public void delete(ProductCategory productCategory){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.delete(productCategory);
        session.getTransaction().commit();
        session.close();
    }
}
