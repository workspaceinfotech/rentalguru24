package com.rentguru24.model;

import com.rentguru24.model.entity.app.UserPaypalCredential;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by mi on 9/23/16.
 */
@Repository
public class UserPaypalCredentialModel extends BaseModel {
    public void insert(UserPaypalCredential userPaypalCredential){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(userPaypalCredential);
        session.getTransaction().commit();
        session.close();
    }
    public UserPaypalCredential getByAppCredentialId(int appCredentialId){
        Session session = this.sessionFactory.openSession();
        try{
            return (UserPaypalCredential)session.createQuery("FROM UserPaypalCredential where appCredential.id=:appCredentialId  ")
                    .setParameter("appCredentialId",appCredentialId)
                    .setMaxResults(1)
                    .uniqueResult();
        }finally {
            session.close();
        }
    }
    public void update(UserPaypalCredential userPaypalCredential){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.update(userPaypalCredential);
        session.getTransaction().commit();
        session.close();
    }
}
