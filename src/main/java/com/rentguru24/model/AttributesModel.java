package com.rentguru24.model;

import com.rentguru24.model.entity.app.Attributes;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by omar on 8/1/16.
 */
@Repository
public class AttributesModel extends BaseModel{
    public void insert(Attributes attributes){
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(attributes);
        session.getTransaction().commit();
        session.close();
    }
    public Attributes getById(){
        Session session = this.sessionFactory.openSession();
        return session.get(Attributes.class, 2);
    }
}
