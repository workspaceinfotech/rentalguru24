package com.rentguru24.controller.web.admin;

import com.rentguru24.helper.ServiceResponse;
import com.rentguru24.model.*;
import com.rentguru24.model.entity.app.AppCredential;
import com.rentguru24.model.entity.app.payments.RentPayment;
import com.rentguru24.model.nonentity.rent_payment.RentPaymentSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * Created by omar on 8/24/16.
 */
@Controller
@RequestMapping("/admin/rent-payment")
public class AdminRentPaymentController {

    @Autowired
    RentPaymentModel rentPaymentModel;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ModelAndView getTransaction(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView("admin/rent-payment/all");
        AppCredential appCredential = (AppCredential) request.getAttribute("appCredential");
        ServiceResponse serviceResponse =(ServiceResponse) request.getAttribute("serviceResponse");
        String baseUrl = (String) request.getAttribute("baseURL");
        Boolean IsLogin = serviceResponse.getResponseStat().getIsLogin();
        HashMap<String, String> breadcrumb = new HashMap<>();


        List<RentPayment> rentPaymentList  = rentPaymentModel.getAll();
        RentPaymentSummary rentPaymentSummary = rentPaymentModel.getSummary();
        modelAndView.addObject("rentPaymentSummary", rentPaymentSummary);
        modelAndView.addObject("rentPaymentList", rentPaymentList);
        modelAndView.addObject("adminUser", appCredential);
        modelAndView.addObject("IsLogIn", IsLogin);
        modelAndView.addObject("BaseUrl", baseUrl);
        modelAndView.addObject("pageHeader", "Banner Image List");
        modelAndView.addObject("breadcrumb", breadcrumb);
        return modelAndView;
    }


}
