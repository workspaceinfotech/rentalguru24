package com.rentguru24.controller.service.app;

import com.fasterxml.jackson.annotation.JsonView;
import com.rentguru24.helper.ServiceResponse;
import com.rentguru24.model.*;
import com.rentguru24.model.entity.BannerImage;
import com.rentguru24.model.entity.app.product.view.ProductView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by mi on 8/8/16.
 */
@RestController
@RequestMapping("/api/banner-image")
public class BannerImageService {
    @Autowired
    BannerImageModel bannerImageModel;

    @JsonView(ProductView.RentalProductView.class)
    @RequestMapping(value = "/get-all", method = RequestMethod.GET)
    public ServiceResponse getProduct(HttpServletRequest request){
        ServiceResponse serviceResponse =(ServiceResponse) request.getAttribute("serviceResponse");
        List<BannerImage> bannerImages = bannerImageModel.getAll();
        serviceResponse.setResponseData(bannerImages,"No product found");
        return serviceResponse;
    }

}

