package com.rentguru24.controller.service.app;


import com.rentguru24.helper.EmailHelper;
import com.rentguru24.controller.service.BaseService;
import com.rentguru24.library.RentGuruMail;
import com.rentguru24.model.*;

import com.rentguru24.model.entity.app.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

/**
 * Created by omar on 8/1/16.
 */
@RestController
@RequestMapping("/api/app")
@Scope("request")
public class TestService extends BaseService{
    @Autowired
    AttributesModel attributesModel;

    @Autowired
    CategoryModel categoryModel;

    @Autowired
    TempFileModel tempFileModel;

    @Autowired
    IdentityTypeModel identityTypeModel;

    @Autowired
    RentRequestModel rentRequestModel;

    @Autowired
    RentPaymentModel rentPaymentModel;

    @Autowired
    RentInfModel rentInfModel;

    @Autowired
    RentGuruMail rentGuruMail;

    @Autowired
    ProductModel productModel;

    @Autowired
    AdminGlobalNotificationModel adminGlobalNotificationModel;

    @Autowired
    AdminGlobalNotificationTemplateModel adminGlobalNotificationTemplateModel;

    @Autowired
    AppLoginCredentialModel appLoginCredentialModel;


    @Autowired
    EmailHelper emailHelper;
    @RequestMapping(value = "/test/category", method = RequestMethod.POST)
    public void postCategory(){
        System.out.println("Init");
          emailHelper.signUpEmailConfirmationMail(appLoginCredentialModel.getAppCredentialById(87), "sd", "sd", "ds");
        System.out.println("Sent");
    }

    @RequestMapping(value = "/test/email", method = RequestMethod.POST)
    public AppCredential testEmail(){
        AppCredential registeredUser = appLoginCredentialModel.getAppCredentialById(80);
        return  registeredUser;

    }
}
