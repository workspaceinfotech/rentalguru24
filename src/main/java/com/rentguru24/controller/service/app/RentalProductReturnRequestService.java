package com.rentguru24.controller.service.app;

import com.rentguru24.helper.DateHelper;
import com.rentguru24.helper.ServiceResponse;
import com.rentguru24.model.ProductModel;
import com.rentguru24.model.RentInfModel;
import com.rentguru24.model.RentalProductReturnRequestModel;
import com.rentguru24.model.entity.app.AppCredential;
import com.rentguru24.model.entity.app.product.rentable.RentInf;
import com.rentguru24.model.entity.app.product.rentable.RentalProductReturnRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by mi on 8/26/16.
 */
@RestController
@RequestMapping("/api/auth/return-request")
public class RentalProductReturnRequestService {
    @Autowired
    RentalProductReturnRequestModel rentalProductReturnRequestModel;
    @Autowired
    RentInfModel rentInfModel;
    @Autowired
    ProductModel productModel;


    @RequestMapping(value = "/make-request/{rentalInfId}",method = RequestMethod.POST)
    public ServiceResponse requestForReturnProductByProductOwner(HttpServletRequest request,
                                                                 @PathVariable int rentalInfId,
                                                                 @RequestParam(value = "remarks",required = false) String remarks){

        ServiceResponse serviceResponse =(ServiceResponse) request.getAttribute("serviceResponse");
        AppCredential appCredential = (AppCredential) request.getAttribute("appCredential");


        RentalProductReturnRequest rentalProductReturnRequest = new RentalProductReturnRequest();
        RentInf rentInf = rentInfModel.getById(rentalInfId);
        if(rentInf==null){
            serviceResponse.setRequestError("rentalInfId","No rent information is found by this rentInfId");
            return serviceResponse;
        }
        if(rentInf.getRentalProduct().getOwner().getId() != appCredential.getId()){
            serviceResponse.setRequestError("rentalInfId","This rent is not belongs to you");
            return serviceResponse;
        }

        if(rentalProductReturnRequestModel.alreadyRequestedToReturn(rentalInfId)){
            serviceResponse.setRequestError("rentalInfId","You have already requested to return your product");
            return serviceResponse;
        }

        rentalProductReturnRequest.setRentInf(rentInf);
        rentalProductReturnRequest.setIsExpired(false);
        rentalProductReturnRequest.setRemarks((remarks == null || remarks.trim().isEmpty()) ? null : remarks);
        rentalProductReturnRequest.setCreatedDate(DateHelper.getCurrentUtcDateTimeStamp());
        rentalProductReturnRequestModel.insert(rentalProductReturnRequest);

        serviceResponse.setResponseData(rentInfModel.getById(rentalInfId));
        return serviceResponse;
    }
}
