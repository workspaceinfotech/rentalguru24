package com.rentguru24.controller.service.app;

import com.fasterxml.jackson.annotation.JsonView;
import com.rentguru24.helper.ServiceResponse;
import com.rentguru24.library.ipGeoTracker.GeoIpManager;
import com.rentguru24.library.ipGeoTracker.dataModel.GeoIp;
import com.rentguru24.model.*;
import com.rentguru24.model.entity.Cities;
import com.rentguru24.model.entity.State;
import com.rentguru24.model.entity.app.*;
import com.rentguru24.model.entity.app.product.rentable.iface.RentalProduct;
import com.rentguru24.model.entity.app.product.view.ProductView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * Created by mi on 8/8/16.
 */
@RestController
@RequestMapping("/api/search/")
public class SearchService {
    @Autowired
    ProductModel productModel;

    @Autowired
    ProductLocationModel productLocationModel;

    @Autowired
    AppLoginCredentialModel appLoginCredentialModel;

    @Autowired
    TempFileModel tempFileModel;

    @Autowired
    CategoryModel categoryModel;

    @Autowired
    RentTypeModel rentTypeModel;

    @Autowired
    ProductRatingModel productRatingModel;

    @Autowired
    RentRequestModel rentRequestModel;

    @Autowired
    StateModel stateModel;

    @Autowired
    CitiesModel citiesModel;

    @RequestMapping(value = "/rental-product",method = RequestMethod.GET)
    @JsonView(ProductView.RentalProductView.class)
    public ServiceResponse searchRentalProduct(
                                            HttpServletRequest request,
                                            @RequestParam(name = "title" ,required = false) String title,
                                            @RequestParam(name = "stateId" ,required = false) Integer stateId,
                                            @RequestParam(name = "cityId" ,required = false) Integer cityId,
                                            @RequestParam(name = "categoryId" ,required = false) Integer categoryId,
                                            @RequestParam(name = "radius" ,required = false) Float radius,
                                            @RequestParam(name = "lat" ,required = false) Double lat,
                                            @RequestParam(name = "lng" ,required = false)Double lng,
                                            @RequestParam(name = "limit" ,required = true)Integer limit,
                                            @RequestParam(name = "offset" ,required = true)Integer offset
                                        ){
        ServiceResponse serviceResponse =(ServiceResponse) request.getAttribute("serviceResponse");
        AppCredential appCredential = (AppCredential) request.getAttribute("appCredential");

        try {
            if(title!=null && !title.equals(""))
                title = URLDecoder.decode(title,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            serviceResponse.setRequestError("title","title is not in valid format");
        }
        if(radius!=null){
            if(lat==null || lng==null){
                GeoIp geoIp = new GeoIpManager().getGeoIp(request);
                if(geoIp!=null){
                    lat = geoIp.getLatitude();
                    lng = geoIp.getLongitude();
                }
            }
        }
        limit=(limit > 10)?10:limit;
        Category category =(categoryId!=null)?categoryModel.getById(categoryId):null;
        State selectedState =(stateId!=null)?stateModel.getById(stateId):null;
        Cities cities =(cityId!=null)?citiesModel.getById(cityId):null;
        List<RentalProduct> rentalProduct = productModel.getRentalProductForSearch(selectedState,cities,category,title,lat,lng,radius,limit,offset);
        serviceResponse.setResponseData(rentalProduct,"No product found");
        return serviceResponse;

    }


}

