package com.rentguru24.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * Created by mi on 8/5/16.
 */

@Service("mailService")
public class RentGuruMail  {

    @Autowired
    private MailSender mailSender;

    public MailSender getMailSender() {
        return mailSender;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendSignUpMail(){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo("rafi101010@gmail.com");
        message.setSubject("TESTING");
        message.setText("BOADY");
        mailSender.send(message);
    }

}
