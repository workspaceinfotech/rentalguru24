package com.rentguru24.helper.template_engine;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.StringWriter;
import java.util.Properties;

/**
 * Created by mi on 12/29/16.
 */

@Component
public class VelocityUtil {


    private static VelocityEngine getVelocityEngine(){

        VelocityEngine ve = new VelocityEngine();
        Properties props = new Properties();
        String folderName = "email-template";
        props.put("file.resource.loader.path", VelocityUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath()+folderName+"/");
        props.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
        ve.init(props);
        return ve;
    }

    public static String getHtmlByTemplateAndContext(String templateName, VelocityContext context){

        VelocityEngine ve = getVelocityEngine();

        Template template = ve.getTemplate(templateName);

        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

    /*public static void main(String[] args) {
        VelocityContext context = new VelocityContext();
        System.out.println(VelocityUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        System.out.println(VelocityUtil.getHtmlByTemplateAndContext("user-signupEmailConfirmation.vm",context));
    }*/
}
