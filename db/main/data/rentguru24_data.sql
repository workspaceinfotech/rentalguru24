-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 06, 2017 at 11:33 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentguru24_develop`
--

--
-- Dumping data for table `admin_cms_page`
--

INSERT INTO `admin_cms_page` (`id`, `page_key`, `page_name`, `page_content`, `sorted_order`, `created_date`) VALUES
(8, 'privacypolicy', 'Privacy Policy', '<p><strong>Privacy Policy Agreement</strong></p>\n\n<p><strong>This Privacy Policy applies to rentguru24.com</strong></p>\n\n<p>www.rentguru24.com in recognises the importance of maintaining your privacy. We value your privacy and appreciate your trust in us. This Policy describes how we treat user information we collect on&nbsp;www.rentguru24.com&nbsp;and other offline sources. This Privacy Policy applies to current and former visitors to our website and to our online customers. By visiting and/or using our website, you agree to this Privacy Policy.</p>\n\n<p><strong>Information We Collect</strong></p>\n\n<p>Contact information. We might collect your Name,Email,Phone,Address,IP Address.</p>\n\n<p>Demographic information. We may collect demographic information about you or any other information provided by your during the use of our website. We might collect this as a part of a survey also.</p>\n\n<p><strong>We Collect Information In Different Ways.</strong></p>\n\n<p>We collect information directly from you. We collect information directly from you when you contact us. We also collect information if you post a comment on our websites or ask us a question through phone or email.</p>\n\n<p><strong>Use Of Your Personal Information</strong></p>\n\n<p>We use information to respond to your requests or questions. We might use your information to confirm your registration for an event or contest.</p>\n\n<p>We use information to improve our products and services. We might use your information to customize your experience with us. This could include displaying content based upon your preferences.</p>\n\n<p>We use information to look at site trends and customer interests. We may use your information to make our website and products better. We may combine information we get from you with information about you we get from third parties.</p>\n\n<p>We use information as otherwise permitted by law.</p>\n\n<p><strong>Email Opt-Out</strong></p>\n\n<p>You can opt out of receiving our marketing emails. To stop receiving our promotional emails, It may take about ten days to process your request. Even if you opt out of getting marketing messages, we will still be sending you transactional messages through email and SMS about your purchases.</p>\n\n<p><strong>Grievance Officer</strong></p>\n\n<p>In accordance with Information Technology Act 2000 and rules made there under, the name and contact details of the Grievance Officer are provided below:</p>\n\n<p><strong>Updates To This Policy</strong></p>\n\n<p>This Privacy Policy was last updated on 1st March, 2016. From time to time we may change our privacy practices. We will notify you of any material changes to this policy as required by law. We will also post an updated copy on our website. Please check our site periodically for updates.</p>\n\n<p><strong>Jurisdiction</strong></p>\n\n<p>If you choose to visit the website, your visit and any dispute over privacy is subject to this Policy and the website&#39;s terms of use. In addition to the foregoing, any disputes arising under this Policy shall be governed by the laws of &nbsp;</p>\n\n<p>&nbsp;</p>\n', 1, '2016-09-09 09:51:41'),
(9, 'termsandcondition', 'Terms  condition', '<p>Terms of Service</p>\n\n<p>This Agreement describes the terms and conditions applicable to user of the &nbsp;rentguru24.com web site, which features tools for trading and communicating in a global business-to-business e-market (the &quot;Service&quot;).</p>\n\n<p>Please read this agreement carefully!</p>\n\n<p>The following describes the Terms and Conditions on which &nbsp;rentguru24.com offers you to access the service.</p>\n\n<p>Acceptance of Terms</p>\n\n<p>By accessing our web site past the home page, you agree that you have entered into this Agreement with &nbsp;rentguru24.com and that you will be bound by the terms and conditions hereof (&quot;Terms&quot;), which &nbsp;rentguru24.com in its discretion may change from time to time. We will post a notice on our web site to inform you of any changes to the Terms. If you do not agree to the changes, you must discontinue using the Service. The changed terms shall automatically be effective immediately after they are initially posted on the homepage as well at Rental / Lease Product offered at our site. Your continuous use of the Service will signify your acceptance of the changed Terms. Unless explicitly stated otherwise, any new features that augment or enhance the Service shall be subject to this Agreement. This Agreement may not be otherwise modified except in a writing signed by an authorized officer of &nbsp;rentguru24.com.</p>\n\n<p>Who Can Use &nbsp;rentguru24.com</p>\n\n<p>The Service is only available to individuals or companies who can form legally binding contracts under applicable law. Thus, you must be at least 18 years of age to use our services. If you do not qualify, please do not use the Service. &nbsp;rentguru24.com may, in its sole discretion, refuse the Service to anyone at any time.</p>\n\n<p>Fees</p>\n\n<p>Visiting for Lease / Rentals offered and this use of the Service is free! However charge fees for additional services asked by you during submitting order, you are responsible for paying all applicable taxes and duties and for all service and other costs you incur for Renting Product from &nbsp;rentguru24.com, procure a listing from us, or access our servers. We reserve the right to change or discontinue, temporarily or permanently, some or all of the Service at any time without notice; &nbsp;rentguru24.com cannot and will not be liable for any loss or damage arising from your failure to comply with steps shown at the time of using Rental services.</p>\n\n<p>Your Information and Items for Rentals</p>\n\n<p>&quot;Your Information&quot; includes any information other users during the Posting your Rental requirements, listing processes, in any public message area or through any email feature. With respect to Your Information, you are solely responsible for Your Information, and we act as a passive conduit for your online distribution and publication of Your Information. We may take any action, however, with respect to your information we deem necessary or appropriate in our sole discretion if we believe it may create liability for us or may cause us to lose (in whole or in part) the services.</p>\n\n<p>Registration Obligations</p>\n\n<p>If you Visit and submit your Rental requirement with us, you agree to:</p>\n\n<ul>\n	<li>Provide true, accurate, current and complete information about yourself or company as prompted by the User/Order form at &nbsp;rentguru24.com, and</li>\n	<li>In case your provided information is untrue, inaccurate, not current or incomplete, &nbsp;rentguru24.com has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete. &nbsp;rentguru24.com has the right to suspend or terminate your order and refuse the current or future use of the Service</li>\n	<li>&nbsp;If you have used our services on behalf of a company or other entity, than you represent and warrant that you have the authority to bind such company or other entity to the Terms.</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>Security for transactions</p>\n\n<p>&nbsp;rentguru24.com is hosted in fully secure server and tie-up for payment gateways also done with secure servers/companies/Banks. Using the Credit Card/Debit Card transactions is fully secure at &nbsp;rentguru24.com. We ensure that &nbsp;rentguru24.com will not disclose the transaction details to any body unless otherwise ask by legal Authorities.</p>\n\n<p>Prohibited Items</p>\n\n<p>You may not post on our service or order through our site any: (a) any item that could cause us to violate any applicable law, status, ordinance or regulation, or (b) any item prohibited and incorporated herein by reference, which may be updated from time to time.</p>\n\n<p>Privacy</p>\n\n<p>&nbsp;rentguru24.com promise to all our customers that the submitted Individual/Company details in accordance with our Privacy Policy, The complete terms of our Privacy Statement are part of this Agreement so you should read it. Please note that when you voluntarily disclose Your Information in &nbsp;rentguru24.com posting areas, that information can be collected and used by others.</p>\n\n<p>Termination</p>\n\n<p>You agree that &nbsp;rentguru24.com, in its sole discretion, may terminate use of the Service, remove and discard Your Information within the Service, for any reason, including without limitation, if &nbsp;rentguru24.com believes that you have violated or acted inconsistently with the letter and spirit of the Agreement. &nbsp;rentguru24.com may also, in its sole discretion and at any time, discontinue providing the Service, or any part thereof, with or without notice. You agree that any termination of your access to the Service under any provision of this Agreement may be effected without prior notice, and acknowledge and agree that &nbsp;rentguru24.com may immediately deactivate or delete your account and all related information and files in your account and/or bar any further access to such files or the Service. Further, you agree that &nbsp;rentguru24.com shall not be liable to you or any third party for any termination of your access to the Service. Paragraphs 6, and 8 shall survive any termination of this Agreement</p>\n\n<p>What Happens if You Break the Rules?</p>\n\n<p>We may immediately issue a warning, temporarily suspend, indefinitely suspend your order, any of current products for rentals, and any other information you place on the site if you breach this Agreement or if we are unable to verify or authenticate any information you provide to us. Without limiting any other remedies, &nbsp;rentguru24.com may suspend if you are found (by conviction, settlement, insurance or escrow investigation, or otherwise) to have engaged in fraudulent activity in connection with our site.</p>\n\n<p>Limit&nbsp;of Liability</p>\n\n<p>You expressly understand and agree that &nbsp;rentguru24.com shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including, damages for loss of profits, goodwill, use, data or other intangible losses (even if &nbsp;rentguru24.com has been advised of the possibility of such damages), resulting from: (i) the use or the inability to use the service; (ii) the cost of procurement of substitute goods and services resulting from any goods, or services purchases or obtained or messages received or transactions entered into through or from the service; (iii) unauthorized access to or alteration of your transmissions or data; (iv) statements or conduct of any third party on the service; or (v) any other matter relating to the service.</p>\n\n<p>Indemnity</p>\n\n<p>You agree to indemnify and hold &nbsp;rentguru24.com and our subsidiaries, affiliates, officers, directors, agents, and employees, harmless from any claim or demand (including legal expenses and the expenses of other professionals) made by a third party due to or arising out of your breach of this Agreement or the documents it incorporates by reference, or your violation of any law or the rights of a third party.</p>\n\n<p>Compliance with Laws</p>\n\n<p>You shall comply with all applicable laws, statutes, ordinances and regulations regarding your use of the Service and, purchase, complying with Indian Laws Subject to Jurisdiction of Hyderabad Courts only.</p>\n\n<p>Notices</p>\n\n<p>Except as explicitly stated otherwise, any notices shall be given by email to the email address you provide to &nbsp;rentguru24.com during the order process (in your case), or such other address as the party shall specify. Notice shall be deemed given twenty-four (24) hours after email is sent, unless the sending party is notified that the email address is invalid. Alternatively, we may give you notice by certified mail, postage prepaid and return receipt requested, to the address provided to &nbsp;rentguru24.com during the registration process. In such case, notice shall be deemed given three (3) days after the date of mailing.</p>\n\n<p>Force Majeure</p>\n\n<p>&nbsp;rentguru24.com shall not be liable to you for delays or failures in performance resulting from causes beyond our reasonable control, including, but not limited to, acts of God, labor disputes or disturbances, material shortages or rationing, riots, acts of war, governmental regulations, communication or utility failures, or casualties.</p>\n\n<p>Assignment</p>\n\n<p>&nbsp;rentguru24.com may assign this Agreement without your consent.</p>\n\n<p>Miscellaneous</p>\n\n<p>The Agreement constitutes the entire Agreement between you and &nbsp;rentguru24.com and controls to use of the Service, superseding any prior written or oral Agreements between you and &nbsp;rentguru24.com. The Indian Laws without regard to its conflict of law provisions shall govern the Agreement in all respects. If any provision of this Agreement is held to be invalid or unenforceable, such provision shall be struck and the remaining provisions shall be enforced. Headings are for reference purposes only and in no way define, limit, construe or describe the scope or extent of such section. Our failure to act with respect to a breach by you or others does not waive our right to act with respect to subsequent or similar breaches.</p>\n\n<p>&nbsp;</p>\n', 2, '2016-09-09 09:52:41'),
(13, 'aboutus', 'about us', '<p><strong>About RentGuru24.com</strong></p>\n\n<p>We are on a Mission to help people in finding their next place where they can stay and live their life Happily.</p>\n', 3, '2016-09-09 11:26:09'),
(17, 'onlinesupport', 'online support', '<p>dfgfd</p>\n', 4, '2016-11-04 10:35:53');

--
-- Dumping data for table `admin_global_notification`
--



--
-- Dumping data for table `admin_global_notification_template`
--

INSERT INTO `admin_global_notification_template` (`id`, `type`, `title`, `template`, `created_date`) VALUES
(1, 'dispute', 'Dispute receive', 'There is product receive dispute complain ', '2016-10-12 10:42:01');

--
-- Dumping data for table `admin_paypal_credential`
--

INSERT INTO `admin_paypal_credential` (`id`, `api_key`, `api_secret`, `created_date`) VALUES
(1, 'AWQr0Ls0qt0zRtXFvSBZ2k3zNgt-0ME5eI6qC8A9dTh2RHodYtDre5cJT7BNElg9mm3dZw6v9F-G-vyn', 'EAiElxCy_o6-h3VKR_iAGIwUVisEUInSXQwbdgRo-Fd8cKUujB2RH86LTXHwOzgUAAGY6Lbm0Nu3kV9q', '2016-09-21 11:47:06');

--
-- Dumping data for table `admin_site_fees`
--

INSERT INTO `admin_site_fees` (`id`, `fixed`, `percentage`, `fixed_value`, `percentage_value`, `created_date`) VALUES
(1, 1, 0, 10.25, 5.25, '2016-08-29 09:03:26');

--
-- Dumping data for table `admin_unread_alert_count`
--

INSERT INTO `admin_unread_alert_count` (`id`, `global_notification`) VALUES
(1, 1);




INSERT INTO `banner_image` (`id`, `image_path`, `url`, `created_date`) VALUES
(9, '1237019901858614.png', 'http://rentguru24.com', '2016-12-20 14:01:31'),
(10, '1237585139572433.jpg', '#', '2016-12-20 14:10:56'),
(11, '1237637659042594.jpg', '#', '2016-12-20 14:11:49');

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `parent_id`, `is_subcategory`, `sorted_order`, `picture`, `product_count`, `created_by`, `created_date`) VALUES
(8, 'Home & Garden', NULL, 0, 1, '{"original":{"path":"2737779563966273.jpg","type":"","size":{"width":500,"height":525}},"thumb":[]}', 23, 0, '2016-12-09 11:55:25'),
(9, 'Cars & Vehicles', NULL, 0, 2, '{"original":{"path":"2738142512716246.jpg","type":"","size":{"width":500,"height":525}},"thumb":[]}', 7, 0, '2016-12-09 11:55:45'),
(10, 'Education', NULL, 0, 3, '{"original":{"path":"2738655553475618.jpg","type":"","size":{"width":500,"height":525}},"thumb":[]}', 1, 0, '2016-12-09 11:56:11'),
(11, 'Clothing, Health & Beauty', NULL, 0, 4, '{"original":{"path":"2738857785454562.jpg","type":"","size":{"width":500,"height":525}},"thumb":[]}', 3, 0, '2016-12-09 11:56:40'),
(12, 'Electronics', NULL, 0, 5, '{"original":{"path":"2737616194130645.jpg","type":"","size":{"width":500,"height":525}},"thumb":[]}', 16, 0, '2016-12-09 11:56:58'),
(13, 'Hobby, Sport & Kids', NULL, 0, 6, '{"original":{"path":"2739116154895649.jpg","type":"","size":{"width":500,"height":525}},"thumb":[]}', 1, 0, '2016-12-09 11:57:15'),
(14, 'Swedish washing machine', 12, 1, 7, NULL, 11, 0, '2016-12-09 11:15:13'),
(22, 'Tools', NULL, 1, 15, NULL, 0, 35, '2016-12-09 11:15:13'),
(23, 'Tool1', NULL, 1, 16, NULL, 0, 35, '2016-12-09 11:15:13'),
(29, 'Appliances', NULL, 1, 17, 'null', 2, 35, '2016-12-09 11:15:13'),
(30, 'Tools', NULL, 1, 18, NULL, 0, 35, '2016-12-09 11:15:13'),
(33, 'Kitchen & Dining', NULL, 1, 20, 'null', 0, 35, '2016-12-09 11:15:13'),
(34, 'Furniture', 8, 1, 21, 'null', 7, 35, '2016-12-09 11:15:13'),
(35, 'asd', NULL, 1, 22, 'null', 0, 35, '2016-12-09 11:15:13'),
(39, 'Electricity, AC, Bathroom & Garden', 8, 1, 23, 'null', 3, 35, '2016-12-09 11:15:13'),
(43, 'Cars', 9, 1, 27, 'null', 4, 35, '2016-12-09 11:15:13'),
(44, 'Motorbikes & Scooters', 9, 1, 28, 'null', 1, 35, '2016-12-09 11:15:13'),
(45, 'Bicycles and Three Wheelers', 9, 1, 29, 'null', 2, 35, '2016-12-09 11:15:13'),
(46, 'Auto Parts & Accessories', 9, 1, 30, 'null', 0, 35, '2016-12-09 11:15:13'),
(47, 'Trucks, Vans & Buses', 9, 1, 31, 'null', 0, 35, '2016-12-09 11:15:13'),
(48, 'Heavy-Duty', 9, 1, 32, 'null', 1, 35, '2016-12-09 11:15:13'),
(49, 'Tuition', 10, 1, 33, 'null', 0, 35, '2016-12-09 11:15:13'),
(50, 'Others Education', 10, 1, 34, 'null', 0, 35, '2016-12-09 11:15:13'),
(51, 'Textbooks', 10, 1, 35, 'null', 0, 35, '2016-12-09 11:15:13'),
(55, 'Clothing', 11, 1, 39, 'null', 1, 35, '2016-12-09 11:15:13'),
(56, 'Health & Beauty Products', 11, 1, 40, 'null', 2, 35, '2016-12-09 11:15:13'),
(57, 'Watches', 11, 1, 41, 'null', 0, 35, '2016-12-09 11:15:13'),
(58, 'Jewelery', 11, 1, 42, 'null', 0, 35, '2016-12-09 11:15:13'),
(60, 'Sports Equipments', 13, 1, 44, 'null', 0, 35, '2016-12-09 11:15:13'),
(61, 'Musical Instruments', 13, 1, 45, 'null', 0, 35, '2016-12-09 11:15:13'),
(62, 'Video Games & Consoles', 13, 1, 46, 'null', 0, 35, '2016-12-09 11:15:13'),
(63, 'Children\'s Items', 13, 1, 47, 'null', 0, 35, '2016-12-09 11:15:13'),
(64, 'Other Hobby, Sport & Kids items', 13, 1, 48, 'null', 0, 35, '2016-12-09 11:15:13'),
(65, 'Mobile Phones & Tablets', 12, 1, 49, 'null', 2, 35, '2016-12-09 11:15:13'),
(66, 'Computers & Laptops', 12, 1, 50, 'null', 1, 35, '2016-12-09 11:15:13'),
(67, 'Computer Accessories', 12, 1, 51, 'null', 2, 35, '2016-12-09 11:15:13'),
(68, 'Cameras & Camcorders', 12, 1, 52, 'null', 0, 35, '2016-12-09 11:15:13'),
(69, 'Television', 12, 1, 53, 'null', 0, 35, '2016-12-09 11:15:13'),
(76, 'Pets & Animals', NULL, 0, 59, '{"original":{"path":"2214324105584638.png","type":"","size":{"width":519,"height":525}},"thumb":[]}', 0, 35, '2016-12-09 11:12:34'),
(77, 'Pets', 76, 1, 60, 'null', 0, 35, '2016-12-09 11:15:13'),
(78, 'Pet & Animal Accessories', 76, 1, 61, 'null', 0, 35, '2016-12-09 11:15:13'),
(79, 'Farm Animals', 76, 1, 62, 'null', 0, 35, '2016-12-09 11:15:13'),
(83, 'Accessories', 12, 1, 66, 'null', 1, 35, '2016-12-09 11:15:13'),
(84, 'Others Electronics', 12, 1, 67, 'null', 0, 35, '2016-12-09 11:15:13'),
(85, 'Water Transport', 9, 1, 68, 'null', 0, 35, '2016-12-09 11:15:13'),
(86, 'Bags', 11, 1, 69, 'null', 0, 35, '2016-12-09 11:15:13'),
(88, 'Shoes & Footwear', 11, 1, 71, 'null', 0, 35, '2016-12-09 11:15:13'),
(89, 'Sunglasses', 11, 1, 72, 'null', 0, 35, '2016-12-09 11:15:13'),
(90, 'Other Personal Items', 11, 1, 73, 'null', 0, 35, '2016-12-09 11:15:13'),
(91, 'Other Fashion Accessories', 11, 1, 74, 'null', 0, 35, '2016-12-09 11:15:13'),
(92, 'Home Appliances', 8, 1, 75, 'null', 8, 35, '2016-12-09 11:15:13'),
(93, 'Other Home Items', 8, 1, 76, 'null', 7, 35, '2016-12-09 11:15:13'),
(95, 'Travel, Events & Tickets', 13, 1, 78, 'null', 1, 35, '2016-12-09 11:15:13'),
(96, 'Music, Books & Movies', 13, 1, 79, 'null', 0, 35, '2016-12-09 11:15:13'),
(97, 'Art & Collectibles', 13, 1, 80, 'null', 0, 35, '2016-12-09 11:15:13'),
(98, 'Handicraft', 13, 1, 81, 'null', 0, 35, '2016-12-09 11:15:13'),
(100, 'Property', NULL, 0, 82, '{"original":{"path":"2739378305391666.jpg","type":"","size":{"width":500,"height":525}},"thumb":[]}', 3, 35, '2016-12-09 11:12:34');

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `state_id`, `city_name`) VALUES
(1, 1, 'Huntsville'),
(2, 1, 'Mobile'),
(3, 1, 'Birmingham'),
(4, 1, 'Montgomery'),
(5, 2, 'Anchorage'),
(6, 3, 'Peoria'),
(7, 3, 'Phoenix'),
(8, 3, 'Chandler'),
(9, 3, 'Gilbert'),
(10, 2, 'Glendale'),
(11, 3, 'Mesa'),
(12, 3, 'Scottsdale'),
(13, 3, 'Surprise'),
(14, 3, 'Tempe'),
(15, 3, 'Tucson'),
(16, 4, 'Little Rock');





--
-- Dumping data for table `rent_type`
--

INSERT INTO `rent_type` (`id`, `name`, `created_date`) VALUES
(1, 'Day', '2016-08-11 13:03:54'),
(2, 'Week', '2016-08-11 13:03:58'),
(3, 'Month', '2016-08-11 13:04:00'),
(4, 'Year', '2016-08-11 13:04:10');

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `code`, `name`, `created_date`) VALUES
(1, 'AL', 'Alabama', '2016-11-29 07:23:28'),
(2, 'AK', 'Alaska', '2016-11-29 07:23:28'),
(3, 'AZ', 'Arizona', '2016-11-29 07:23:28'),
(4, 'AR', 'Arkansas', '2016-11-29 07:23:28'),
(5, 'CA', 'California', '2016-11-29 07:23:28'),
(6, 'CO', 'Colorado', '2016-11-29 07:23:28'),
(7, 'CT', 'Connecticut', '2016-11-29 07:23:28'),
(8, 'DE', 'Delaware', '2016-11-29 07:23:28'),
(9, 'DC', 'District of Columbia', '2016-11-29 07:23:28'),
(10, 'FL', 'Florida', '2016-11-29 07:23:28'),
(11, 'GA', 'Georgia', '2016-11-29 07:23:28'),
(12, 'HI', 'Hawaii', '2016-11-29 07:23:28'),
(13, 'ID', 'Idaho', '2016-11-29 07:23:28'),
(14, 'IL', 'Illinois', '2016-11-29 07:23:28'),
(15, 'IN', 'Indiana', '2016-11-29 07:23:28'),
(16, 'IA', 'Iowa', '2016-11-29 07:23:28'),
(17, 'KS', 'Kansas', '2016-11-29 07:23:28'),
(18, 'KY', 'Kentucky', '2016-11-29 07:23:28'),
(19, 'LA', 'Louisiana', '2016-11-29 07:23:28'),
(20, 'ME', 'Maine', '2016-11-29 07:23:28'),
(21, 'MD', 'Maryland', '2016-11-29 07:23:28'),
(22, 'MA', 'Massachusetts', '2016-11-29 07:23:28'),
(23, 'MI', 'Michigan', '2016-11-29 07:23:28'),
(24, 'MN', 'Minnesota', '2016-11-29 07:23:28'),
(25, 'MS', 'Mississippi', '2016-11-29 07:23:28'),
(26, 'MO', 'Missouri', '2016-11-29 07:23:28'),
(27, 'MT', 'Montana', '2016-11-29 07:23:28'),
(28, 'NE', 'Nebraska', '2016-11-29 07:23:28'),
(29, 'NV', 'Nevada', '2016-11-29 07:23:28'),
(30, 'NH', 'New Hampshire', '2016-11-29 07:23:28'),
(31, 'NJ', 'New Jersey', '2016-11-29 07:23:28'),
(32, 'NM', 'New Mexico', '2016-11-29 07:23:28'),
(33, 'NY', 'New York', '2016-11-29 07:23:28'),
(34, 'NC', 'North Carolina', '2016-11-29 07:23:28'),
(35, 'ND', 'North Dakota', '2016-11-29 07:23:28'),
(36, 'OH', 'Ohio', '2016-11-29 07:23:28'),
(37, 'OK', 'Oklahoma', '2016-11-29 07:23:28'),
(38, 'OR', 'Oregon', '2016-11-29 07:23:28'),
(39, 'PA', 'Pennsylvania', '2016-11-29 07:23:28'),
(40, 'PR', 'Puerto Rico', '2016-11-29 07:23:28'),
(41, 'RI', 'Rhode Island', '2016-11-29 07:23:28'),
(42, 'SC', 'South Carolina', '2016-11-29 07:23:28'),
(43, 'SD', 'South Dakota', '2016-11-29 07:23:28'),
(44, 'TN', 'Tennessee', '2016-11-29 07:23:28'),
(45, 'TX', 'Texas', '2016-11-29 07:23:28'),
(46, 'UT', 'Utah', '2016-11-29 07:23:28'),
(47, 'VT', 'Vermont', '2016-11-29 07:23:28'),
(48, 'VA', 'Virginia', '2016-11-29 07:23:28'),
(49, 'WA', 'Washington', '2016-11-29 07:23:28'),
(50, 'WV', 'West Virginia', '2016-11-29 07:23:28'),
(51, 'WI', 'Wisconsin', '2016-11-29 07:23:28'),
(52, 'WY', 'Wyoming', '2016-11-29 07:23:28');

--
-- Dumping data for table `temp_file`
--

--
-- Dumping data for table `user_address`
--

INSERT INTO `identity_type` (`id`, `name`, `created_date`) VALUES
  (1, 'Passport', '2016-08-05 05:19:24'),
  (2, 'National ID', '2016-08-03 08:02:10');# 2 rows affected.


INSERT INTO `user_address` (`id`, `address`, `zip`, `city`, `state`, `lat`, `lng`, `created_date`) VALUES
  (1, 'sdf', 'sdf', 'sdf', 'sdf', NULL, NULL, '2016-08-02 12:41:56'),
  (2, '', '', '', '', NULL, NULL, '2016-08-04 12:47:12'),
  (3, '', '', '', '', NULL, NULL, '2016-08-04 12:53:50');# 3 rows affected.



INSERT INTO `user_inf` (`id`, `user_address_id`, `identity_type_id`, `first_name`, `last_name`, `phone_number`, `gender`, `profile_pic`, `identity_doc_path`, `created_date`) VALUES
  (1, 1, 1, 'Mausum', 'N', '', 'male', '{"original":{"path":"32/4999332773575341.JPG","type":"","size":{"width":3000,"height":2002}},"thumb":[]}', 'identityDoc/41/183824984841829.png', '2017-01-19 05:41:41'),
  (2, 2, 1, 'Taiful', 'Islam', '', 'male', NULL, 'identityDoc/41/183824984841829.png', '2017-01-19 05:41:41'),
  (3, 3, 1, 'developer', 'wsit', '', 'male', NULL, 'identityDoc/34/14499115145635.jpg', '2017-01-19 05:41:41');# 3 rows affected.


INSERT INTO `app_login_credential` (`id`, `user_inf_id`, `role`, `email`, `password`, `accesstoken`, `varified`, `email_confirmed`, `blocked`, `created_date`) VALUES
  (1, 1, -1, 'mousum@workspace.com', '$2a$10$ffbXEtKzwBUNcH3K01o9Z.a1iksYDTr72wNvgtmhlXDwLmwP5K7Vq', 'cb8176894ad9985c46a0d8ff98de39e4818e1cd6c5506da0b1a2ebcb3c510abe32', 1, 1, 0, '2016-11-08 07:22:52'),
  (2, 2, -1, 'tomal@gmail.com', '$2a$10$z4aOz5eIWcEGHXivvl9SiOhibdqneT2n/NI41uQS2f1usTuSuAQ62', '5f08323684a25d460fbd4a574f64af96', 1, 1, 0, '2016-11-08 07:22:52'),
  (3, 3, -1, 'developer@wsit.com', '$2a$10$z4aOz5eIWcEGHXivvl9SiOhibdqneT2n/NI41uQS2f1usTuSuAQ62', '84e2761bca190cb87d9e0306f27b6c82', 1, 1, 0, '2016-11-08 07:22:52');# 3 rows affected.




--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `owner_id`, `name`, `description`, `average_rating`, `rating`, `profile_image`, `other_images`, `current_value`, `rent_fee`, `rent_type_id`, `active`, `currently_available`, `available_from`, `available_till`, `review_status`, `created_date`) VALUES
  (1, 1, 'Technic Electric Beauty 12 Bright Eyeshadow Palette', 'Contains 12 vibrant eye shadow You can create an array of stunningly radiant looks that will make your peepers stand out A dramatic look Simply pick your favourite shade and build up the shadow in layers until you get the intensity you desire', 2.10, 0, '{"original":{"path":"product/39/111245506866506.jpg","type":"","size":{"width":1600,"height":1200}},"thumb":[]}', '[]', 5.00, 2.00, 1, 1, 0, '2016-10-24 00:00:00', '2017-10-25 00:00:00', 1, '2016-12-09 11:18:12'),
  (2, 1, 'Restaurant', 'Restaurant for rent', 4.00, 0, '{"original":{"path":"product/32/256608369576878.jpg","type":"","size":{"width":550,"height":364}},"thumb":[]}', '[]', 5.00, 2.00, 2, 3, 1, '2016-10-24 00:00:00', '2016-10-25 00:00:00', 1, '2016-12-09 11:18:12'),
  (3, 2, 'Audi Bicycle', 'Audi Bicycle for rent', 3.00, 0, '{"original":{"path":"product/32/259756689545007.jpg","type":"","size":{"width":1024,"height":768}},"thumb":[]}', '[]', 5.00, 2.00, 1, 1, 1, '2016-10-24 00:00:00', '2016-11-30 00:00:00', 1, '2016-09-26 05:17:30');
--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `product_id`, `category_id`, `created_date`) VALUES
  (1, 1, 9, '2016-08-10 12:53:12'),
  (2, 2, 14, '2016-08-12 08:02:09'),
  (3, 3, 8, '2016-08-12 08:05:32');

--
-- Dumping data for table `product_location`
--

INSERT INTO `product_location` (`id`, `product_id`, `state_id`, `city_id`, `formatted_address`, `zip`, `lat`, `lng`, `created_date`) VALUES
  (1, 1, 1, NULL, 'Nikunja', '1234', 23.828155400, 90.417240200, '2016-10-21 11:02:37'),
  (2, 2, 1, NULL, 'Banani', 'wdf', 23.828155400, 90.417240200, '2016-10-21 11:02:37'),
  (3, 3, 1, NULL, 'Banani', 'wdf', 23.828155400, 90.417240200, '2016-10-21 11:02:37');




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
